# OneR Weka API

OneR Weka API is an API made to classify subgroups of Systemic Sclerosis (SSC), it labels unknown instances as Healthy, DSSc and LSSc. To do this it requires an test dataset in the form of an ARFF file.

## Getting Started

* Fork this repository into your own Bitbucket account (You can find this option in the Actions submenu of the Bitbucket repo: the three dots in the right menu)

* Clone the forked repository from your Bitbucket account to your local computer.

* Open the project in IntelliJ

### Prerequisites

Add Weka (>3.5.5) and commons-cli to your build path. In build.gradle it is something like this:

```
// https://mvnrepository.com/artifact/nz.ac.waikato.cms.weka/weka-stable
compile group: 'nz.ac.waikato.cms.weka', name: 'weka-stable', version: '3.8.0'
compile('commons-cli:commons-cli:1.4')
```

## Deployment

Deploy the program with an ARFF file as command line argument.

## Built With

* [CLI](https://commons.apache.org/proper/commons-cli/) - The command line client

## Versioning

When using git versioning, you should create a .gitignore file with something like below in it! Adjust this to your needs, of course.

```
.gradle
/build/
/.idea
/gradle
gradlew
gradlew.bat
CLIdemo.iml

*.class
*.log
*.jar

```

## Authors

* **Yaprak Yigit** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
