/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.yyigit;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * This class implements the OptionsProvider interface by parsing the passed command line arguments.
 *
 * @author Yaprak Yigit
 */
public class ApacheCliOptionsProvider  {
        private static final String HELP = "help";
        private static final String TESTFILE = "test file";

        private final String[] clArguments;
        private Options options;
        private CommandLine commandLine;


        /**
         * constructs with the command line array.
         *
         * @param args the CL array
         */
        public ApacheCliOptionsProvider(final String[] args) {
            this.clArguments = args;
            initialize();
        }

        /**
         * Options initialization and processing.
         */
        private void initialize() {
            buildOptions();
            processCommandLine();
        }

        /**
         * Check if help was requested; if so, return true.
         * @return helpRequested
         */
        public boolean helpRequested() {
            return this.commandLine.hasOption(HELP);
        }

        public void buildOptions() {
            // Create Options object
            this.options = new Options();
            // New options
            Option helpOption = new Option("h", HELP, false, "Prints this message");
            Option fileOption = new Option("f", TESTFILE, true, "Test file with unknown instances");
            // Add options
            options.addOption(helpOption);
            options.addOption(fileOption);
        }

    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);

        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    public String getTestFile(){
        String testFile = commandLine.getOptionValue(TESTFILE);
        if (testFile.endsWith(".arff")) {
            // do something
            return commandLine.getOptionValue("f");
        } else {
            throw new IllegalArgumentException("Test file argument is not legal: \"" + testFile + "\"");
        }

    }
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("help", options);
    }


}
